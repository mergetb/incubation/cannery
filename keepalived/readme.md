# The Mighty Front Door

This example demonstrates how to create a resilient front door for your testbed site. We use a pair of servers with a floating IP address as the front door (tier1) and assume you have a scale-out back end (tier2). The servers keep tabs on one another through a pair of technologies called [keepalived](http://keepalived.org) and [VRRP](https://tools.ietf.org/html/rfc3768). One server is designated as the master and the other as a backup. When the server fails, or the [HAProxy](https://haproxy.org) running on the master fails, the backup automatically takes over.

![](diagram.png)

## Running

You'll need to have [Raven](https://gitlab.com/rygoo/raven#installing) installed.

```shell
sudo su

# build and deploy the raven topology
rvn build
rvn deploy

# wait for all IP addresses to appear in `rvn status`

# run the base topology configuration
rvn configure
ansible-playbook -i .rvn/ansible-hosts network.yml

# set up the first tier
ansible-playbook -i .rvn/ansible-hosts tier1.yml

# set up the second tier
ansible-playbook -i .rvn/ansible-hosts tier2.yml
```

## Testing

Log into the user and continually scrape the floating tier-1 IP address.

```shell
eval $(rvn ssh user)

[rvn@user ~]$ while true; do curl 10.0.0.100; echo; sleep1; done
<h1>whiskey</h1>
<h1>tango</h1>
<h1>foxtrot</h1>
```

Leave that running and open up a new shell and log into `alpha`.

```shell
eval $(rvn ssh alpha)

# note that the floating ip is here
ip addr
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP ... 
inet 10.0.0.100/32 scope global eth1
   valid_lft forever preferred_lft forever

# take down eth1 and notice a brief pause in the user curls but that it picks back up again after a few seconds
ip link set down dev eth1
```

```shell
eval $(rvn ssh bravo)

# note that the floating ip now is here
ip addr
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP ... 
inet 10.0.0.100/32 scope global eth1
   valid_lft forever preferred_lft forever

# take down eth1 and notice a brief pause in the user curls but that it picks back up again after a few seconds
ip link set down dev eth1
```

If you bring the interface back up on `alpha`, the floating IP will disappear from `bravo` and return to `alpha`. Study the Ansible files to see whats going on under the hood. 

**glhf**
