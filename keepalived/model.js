user = fedora('user');

tier1 = [
  fedora('alpha'),
  fedora('bravo')
];

tier2 = [
  fedora('whiskey'),
  fedora('tango'),
  fedora('foxtrot')
];

sw = cumulus('sw');

topo = {
  name: 'keepalived',
  nodes: [user, ...tier1, ...tier2],
  switches: [sw],
  links: [
    Link('user', 1, 'sw', 1),
    Link('alpha', 1, 'sw', 2),
    Link('bravo', 1, 'sw', 3),
    Link('whiskey', 1, 'sw', 4),
    Link('tango', 1, 'sw', 5),
    Link('foxtrot', 1, 'sw', 6)
  ]
};

function fedora(name) {
  return {
    name: name,
    image: 'fedora-28',
  }
}

function cumulus(name) {
  return {
    name: name,
    image: 'cumulusvx-3.5-mvrf'
  }
}
