# Highly Available Elasticsearch

This example demonstrates how to setup a 3 node Elasticsearch cluster, and how to create search indices with two redundant replicas to let you fail all the way down to one Elasticsearch instance and still be operational. [Elasticsearch](https://www.elastic.co/) combined with [Kibana](https://www.elastic.co/products/kibana) is an extremely useful tool for log management. Logging is what we use this combo for in the Merge portal. We've put this canned example here for Merge sites that may be interested in having a highly-available logging backend. All the core service log to both syslog and Elasticsearch.

![](diagram.png)

## Running

You'll need to have [Raven](https://gitlab.com/rygoo/raven#installing) installed.

```shell
sudo su

# build and deploy the raven topology
rvn build
rvn deploy

# wait for all IP addresses to appear in `rvn status`

# run the base topology configuration
rvn configure
ansible-playbook -i .rvn/ansible-hosts network.yml

# set elasticsearch, kibana and haproxy (for distributing requests across elasticsearch servers)
ansible-playbook -i .rvn/ansible-hosts elastic-kibana.yml

```

## Testing

Log into the app, and see that we can hit the Elasticsearch API.

```shell
$ eval $(rvn ssh app)

[rvn@app ~]$ curl 10.0.0.100:9200 | jq
{
  "name" : "whiskey",
  "cluster_name" : "mergetb",
  "cluster_uuid" : "esr0JLA5Rjet5Yir1eAYLQ",
  "version" : {
    "number" : "6.5.0",
    "build_flavor" : "default",
    "build_type" : "rpm",
    "build_hash" : "816e6f6",
    "build_date" : "2018-11-09T18:58:36.352602Z",
    "build_snapshot" : false,
    "lucene_version" : "7.5.0",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```

If you run this command consecutively, you'll see that the proxy is rotating through the backends.

Use the cluster health script to see the `cluster-health` script to see what the initial health of the cluster is.

```shell
[rvn@app ~]$ ./cluster-health.sh
{
  "cluster_name": "mergetb",
  "status": "green",
  "timed_out": false,
  "number_of_nodes": 3,
  "number_of_data_nodes": 3,
  "active_primary_shards": 1,
  "active_shards": 2,
  "relocating_shards": 0,
  "initializing_shards": 0,
  "unassigned_shards": 0,
  "delayed_unassigned_shards": 0,
  "number_of_pending_tasks": 0,
  "number_of_in_flight_fetch": 0,
  "task_max_waiting_in_queue_millis": 0,
  "active_shards_percent_as_number": 100
}
```

Now lets create an index with 2 redundant replicas. And post some data to it

```shell
[rvn@app ~]$ ./create-muffin-index.sh
{
  "acknowledged": true,
  "shards_acknowledged": true,
  "index": "muffins"
}

[rvn@app ~]$ ./post-data.sh
{
  "_index": "muffins",
  "_type": "_doc",
  "_id": "1",
  "_version": 1,
  "result": "created",
  "_shards": {
    "total": 3,
    "successful": 3,
    "failed": 0
  },
  "_seq_no": 0,
  "_primary_term": 1
}
```

We can now query Elasticsearch

```shell
[rvn@app ~]$ ./query-data.sh
{
  "took": 18,
  "timed_out": false,
  "_shards": {
    "total": 3,
    "successful": 3,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 1,
    "max_score": 0.2876821,
    "hits": [
      {
        "_index": "muffins",
        "_type": "_doc",
        "_id": "1",
        "_score": 0.2876821,
        "_source": {
          "from": "murphy",
          "question": "do you know the muffin man?"
        }
      }
    ]
  }
}
```

Now to the fun part, lets destroy one of the servers and see how the system responds. This series of commands starts back on the host. First try curling the API, you'll notice that we're only getting responses from whiskey and foxtrot now.

```shell
$ rvn wipe tango

$ eval $(rvn ssh app)

[rvn@app ~]$ curl 10.0.0.100:9200 | jq
{
  "name" : "whiskey",
  "cluster_name" : "mergetb",
  "cluster_uuid" : "esr0JLA5Rjet5Yir1eAYLQ",
  "version" : {
    "number" : "6.5.0",
    "build_flavor" : "default",
    "build_type" : "rpm",
    "build_hash" : "816e6f6",
    "build_date" : "2018-11-09T18:58:36.352602Z",
    "build_snapshot" : false,
    "lucene_version" : "7.5.0",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}

```

Next let's look at the cluster health. It takes Elasticsearch a bit to realize it's lost one of it's members. But eventually you'll see the following degraded state. Notice that the `status` is now yellow and the `active_shards_percent_as_number` is `63.63`.

```shell
[rvn@app ~]$ ./cluster-health.sh 
{
  "cluster_name": "mergetb",
  "status": "yellow",
  "timed_out": false,
  "number_of_nodes": 2,
  "number_of_data_nodes": 2,
  "active_primary_shards": 4,
  "active_shards": 7,
  "relocating_shards": 0,
  "initializing_shards": 0,
  "unassigned_shards": 4,
  "delayed_unassigned_shards": 4,
  "number_of_pending_tasks": 0,
  "number_of_in_flight_fetch": 0,
  "task_max_waiting_in_queue_millis": 0,
  "active_shards_percent_as_number": 63.63636363636363
}
```
You can kill off one more server leaving only one standing and query will still work. Note that this is a function of how the index was set up. Check out the [create-muffin-index.sh](create-muffin-index.sh) script for the details.



**glhf**
