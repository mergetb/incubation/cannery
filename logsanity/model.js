topo = {
  name: 'logsanity',
  nodes: [f28('app'), f28('haproxy'), f28('whiskey'), f28('tango'), f28('foxtrot')],
  switches: [cumulus('sw')],
  links: [
    Link('app', 1, 'sw', 1),
    Link('haproxy', 1, 'sw', 2),
    Link('whiskey', 1, 'sw', 3),
    Link('tango', 1, 'sw', 4),
    Link('foxtrot', 1, 'sw', 5)
  ]
}

function f28(x) {
  return { name: x, image: 'fedora-28', memory: { capacity: GB(2) } }
}

function cumulus(name) {
  return { name: name, image: 'cumulusvx-3.5-mvrf' };
}
