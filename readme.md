# Cannery

This repository contains canned examples of technology deployments. The intent is to provide stepping stones for using these technologies to build testbed services. All of the examples in this repository use the [Raven](https://gitlab.com/rygoo/raven) topology virtualization tool.

