# IPSec Control Net Access

This example demonstrates how to create VPN access to experiment control nets using the strongSwan IPsec implementation. This is useful for

- Connecting experimenter workstations to experiment control networks.
- Connecting cloud resources to experiment control networks.
- Connecting build or development clusters to experiment control networks.

The canned experiment setup is depicted below.

![](diagram.png)

The features of this can are the following

- strongSwan IPsec configuration and deployment
- IPsec to VLAN bridging


## Running

You'll need to have [Raven](https://gitlab.com/rygoo/raven#installing) installed.

```shell
sudo su

# build and deploy the raven topology
rvn build
rvn deploy

# wait for all IP addresses to appear in `rvn status`

# run the base topology configuration
rvn configure
ansible-playbook -i .rvn/ansible-hosts network.yml

# setup strongSwan ipsec
ansible-playbook -i .rvn/ansible-hosts strongswan.yml
```

Then you can ssh around with `eval $(rvn ssh <node>)` and watch things go. Study the ansible code to in combination with observation to learn how things work.

## Testing

To test VPN/VLAN A

```
eval $(rvn ssh a0)
# drops into host-a

sudo su
strongswan up ikev2-rw

# ping a node in VLAN A on the other side of the IPsec tunnel
ping 10.10.10.11
```

To test VPN/VLAN B

```
eval $(rvn ssh b0)
# drops into host-b

sudo su
strongswan up ikev2-rw

# ping a node in VLAN A on the other side of the IPsec tunnel
ping 10.10.10.12
```
