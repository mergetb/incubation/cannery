
let a0 = node('a0'),
    b0 = node('b0'),
    gw = node('gw'),
    a1 = node('a1'),
    b1 = node('b1'),
    b2 = node('b2'),
    internet = node('internet'),
    clsw = sw('clsw'),
    ctsw = sw('ctsw');

topo = {
  name: 'ipsecvlan',
  nodes: [a0, b0, gw, internet, a1, b1, b2],
  switches: [clsw, ctsw],
  links: [
    Link('a0', 0, 'clsw', 1),
    Link('b0', 0, 'clsw', 2),
    Link('clsw', 3, 'internet', 0),
    Link('internet', 1, 'gw', 0),
    Link('gw', 1, 'ctsw', 0),
    Link('ctsw', 1, 'a1', 0),
    Link('ctsw', 2, 'b1', 0),
    Link('ctsw', 2, 'b2', 0)
  ]
}

function node(name) {
  return { 
    name: name, 
    image: 'fedora-28',
    mounts: [{ source: env.PWD+'/scratch', point: '/tmp/scratch'}]
  };
}

function sw(name) {
  return { name: name, image: 'cumulusvx-3.5-mvrf' };
}

